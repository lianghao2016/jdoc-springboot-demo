package com.nmtx.springboot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户模块
 * 
 * @author lianghao
 * @version 1.0.0
 * 
 */
@RestController
@RequestMapping("/user")
public class UserController {
   
    
    /**
     * 用于添加用户功能
     * @title 新增用户
     * @param User
     * @param {"username":"admin","password":"123456"}
     * @return {"code":"100000","data":"","message":"新增成功"}
     */
    @RequestMapping("/add")
    public String add(User user){
       
        return "{\"code\":\"100000\",\"data\":\"\",\"message\":\"新增成功\"}";
    }
    
    
    /**
     * 用于删除用户功能
     * @title 删除用户
     * @param id|用户id|Intger|必填
     * @return {"code":"100000","data":"","message":"删除成功"}
     */
    @RequestMapping(value="/delete",method=RequestMethod.POST)
    public String delete(){
       
        return "{\"code\":\"100000\",\"data\":\"\",\"message\":\"删除成功\"}";
    }
    
    /**
     * 通过用户id查询用户功能
     * @title 查询ID查用户
     * @param username|用户名|String|必填
     * @param password|密码|String|必填
     * @return {"code":"100000","data":{"password":"123456","username":"13811111111"},"message":"获取成功"}
     */
    @RequestMapping("/getUserById")
    public String getUserById(){
        return "{\"code\":\"100000\",\"data\":{\"password\":\"123456\",\"username\":\"13811111111\"},\"message\":\"获取成功\"}";
    }
    
}