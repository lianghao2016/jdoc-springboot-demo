package com.nmtx.springboot;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jdocapi.doc.core.api.springmvc.SpringMVCApiDocConfig;



@SpringBootApplication
public class Application {
    
    public static void main(String[] args) throws Exception {
		new SpringMVCApiDocConfig()
		.scanPackage(Application.class.getPackage().getName())
		.buildWeb("ff0053dad7c89759e6069d1fdce6a6ae").start();
       
    }
}
